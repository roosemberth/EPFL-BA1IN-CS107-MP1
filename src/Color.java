
/**
 * @author Roosembert Palacios
 * @author Thomas Adam
 */
public final class Color {
	
	/**
     * Limit a value between two bounds
     * @param val float value to sanitize
     * @param min float lower bound
     * @param max float upper bound
     * @return a float between min and max
     */
	private static float limitValue(float val, float min, float max){
		if (val>max)
			return max;
		if (val<min)
			return min;
		return val;
	}

    /**
     * Returns red component from given packed color.
     * @param rgb 32-bits RGB color
     * @return a float between 0.0 and 1.0
     * @see #getGreen
     * @see #getBlue
     * @see #getRGB(float, float, float)
     */
    public static float getRed(int rgb) {
        return (float) ((1 / 255.0) * ((rgb & 0xff0000) >> 16));
    }

    /**
     * Returns green component from given packed color.
     * @param rgb 32-bits RGB color
     * @return a float between 0.0 and 1.0
     * @see #getRed
     * @see #getBlue
     * @see #getRGB(float, float, float)
     */
    public static float getGreen(int rgb) {
        return (float) ((1 / 255.0) * ((rgb & 0x00ff00) >> 8));
    }

    /**
     * Returns blue component from given packed color.
     * @param rgb 32-bits RGB color
     * @return a float between 0.0 and 1.0
     * @see #getRed
     * @see #getGreen
     * @see #getRGB(float, float, float)
     */
    public static float getBlue(int rgb) {
        return (float) ((1 / 255.0) * ((rgb & 0x0000ff) >> 0));
    }
    
    /**
     * Returns the average of red, green and blue components from given packed color.
     * @param rgb 32-bits RGB color
     * @return a float between 0.0 and 1.0
     * @see #getRed
     * @see #getGreen
     * @see #getBlue
     * @see #getRGB(float)
     */
    public static float getGray(int rgb) {
    	return (float) ((1/3.0)*(getRed(rgb)+getGreen(rgb)+getBlue(rgb)));
    }

    /**
     * Returns packed RGB components from given red, green and blue components.
     * @param red a float between 0.0 and 1.0
     * @param green a float between 0.0 and 1.0
     * @param blue a float between 0.0 and 1.0
     * @return 32-bits RGB color
     * @see #getRed
     * @see #getGreen
     * @see #getBlue
     */
    public static int getRGB(float red, float green, float blue) {
    	// Define Limits
    	float min = 0;
    	float max = 1;
    	// Sanitize values
    	float myRed = limitValue(red, min, max);
    	float myGreen = limitValue(green, min, max);
    	float myBlue = limitValue(blue, min, max);
    	// Actual Calculation
    	int RGB = ((((int)(myRed*255))<<16) | (((int)(myGreen*255))<<8) | (((int)(myBlue*255))<<0));

        return RGB;
    }
    
    /**
     * Returns packed RGB components from given grayscale value.
     * @param gray a float between 0.0 and 1.0
     * @return 32-bits RGB color
     * @see #getGray
     */
    public static int getRGB(float gray) {
    	return getRGB(gray, gray, gray);
    }

	/**
     * Converts packed RGB image to grayscale float image.
     * @param image a HxW int array
     * @return a HxW float array
     * @see #toRGB
     * @see #getGray
     */
    public static float[][] toGray(int[][] image) {
    	float [][] gray = new float[image.length][image[0].length];
    	for (int i = 0; i < image.length; ++i){
    		for (int j=0; j<image[i].length; ++j){
    			gray[i][j]=getGray(image[i][j]);
    		}
    	}
        return gray;
    }

    /**
     * Converts grayscale float image to packed RGB image.
     * @param channels a HxW float array
     * @return a HxW int array
     * @see #toGray
     * @see #getRGB(float)
     */
    public static int[][] toRGB(float[][] gray) {
    	int [][] color = new int[gray.length][gray[0].length];
    	for (int i = 0; i < gray.length; ++i){
    		for (int j=0; j<gray[i].length; ++j){
    			color[i][j]=getRGB(gray[i][j]);
    		}
    	}
        return color;
    }

}
