
/**
 * @author Roosembert Palacios 
 * @author Thomas Adam
 */
public final class Filter {
	
	/**
     * Limit a value between two bounds
     * @param val int value to sanitize
     * @param min int lower bound
     * @param max int upper bound
     * @return an int between min and max
     */
	private static int limitValue(int val, int min, int max){
		if (val>max)
			return max;
		if (val<min)
			return min;
		return val;
	}

    /**
     * Get a pixel without accessing out of bounds
     * @param gray a HxW float array
     * @param row Y coordinate
     * @param col X coordinate
     * @return nearest valid pixel color
     */
    public static float at(float[][] gray, int row, int col) {
    	// Sanitize inputs
    	int myRow=limitValue(row, 0, gray.length-1);
    	int myCol=limitValue(col, 0, gray[myRow].length-1);
        return gray[myRow][myCol];
    }

    /**
     * Convolve a single-channel image with specified kernel.
     * @param gray a HxW float array
     * @param kernel a MxN float array, with M and N odd
     * @return a HxW float array
     */
    public static float[][] filter(float[][] gray, float[][] kernel) {
    	// Kernel neighborhood size = {a,b} for kernel size of [2a+1][2b+1]
    	int []kns = {(kernel.length-1)/2, (kernel[0].length-1)/2};
    	float [][] convoluted = new float[gray.length][gray[0].length];
    	for (int i = 0; i < gray.length; ++i){
    		for (int j=0; j<gray[0].length; ++j){
    			convoluted[i][j]=0;
    			// iterate on first dimension from -a to a with 2a+1 the size of the first dimension of the kernel
    			for (int k = -kns[0]; k <= kns[0]; ++k){
    				// iterate on second dimension from -b to b with 2b+1 the size of the second dimension of the kernel
    				for (int l=-kns[1]; l<=kns[1]; ++l){
    					convoluted[i][j] += at(gray,i+k,j+l)*kernel[k+kns[0]][l+kns[1]];
    				}
    			}
    		}
    	}
        return convoluted;
    }

    /**
     * Smooth a single-channel image
     * @param gray a HxW float array
     * @return a HxW float array
     */
    public static float[][] smooth(float[][] gray) {
    	final float [][]smoothKernel = {
    			{1/10f,1/10f,1/10f},
    			{1/10f,2/10f,1/10f},
    			{1/10f,1/10f,1/10f}
    	};
        return filter(gray, smoothKernel);
    }

    /**
     * Compute horizontal Sobel filter
     * @param gray a HxW float array
     * @return a HxW float array
     */
    public static float[][] sobelX(float[][] gray) {
    	final float [][]sobelxFilter = {
    			{-1,0,+1},
    			{-2,0,+2},
    			{-1,0,+1}
    	};
        return filter(gray, sobelxFilter);
    }

    /**
     * Compute vertical Sobel filter
     * @param gray a HxW float array
     * @return a HxW float array
     */
    public static float[][] sobelY(float[][] gray) {
    	final float [][]sobelyFilter = {
    			{-1,-2,-1},
    			{ 0, 0, 0},
    			{+1,+2,+1}
    	};
        return filter(gray, sobelyFilter);
    }

    /**
     * Compute the magnitude of combined Sobel filters
     * @param gray a HxW float array
     * @return a HxW float array
     */
    public static float[][] sobel(float[][] gray) {
    	float [][] sobeledImage = new float[gray.length][gray[0].length];
    	float [][] sobeledX = sobelX(gray);
    	float [][] sobeledY = sobelY(gray);
    	for (int i = 0; i < gray.length; ++i){
    		for (int j=0; j<gray[0].length; ++j){
    			sobeledImage[i][j]=(float)Math.sqrt(Math.pow(sobeledX[i][j], 2)+Math.pow(sobeledY[i][j], 2));
    		}
    	}
        return sobeledImage;
    }

}
