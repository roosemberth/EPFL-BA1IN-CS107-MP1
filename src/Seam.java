/**
 * @author Roosembert Palacios 
 * @author Thomas Adam
 */
public final class Seam {

	private static int limitValue(int val, int min, int max){
		if (val>max)
			return max;
		if (val<min)
			return min;
		return val;
	}

	/**
	 * Compute shortest path between {@code from} and {@code to}
	 * @param successors adjacency list for all vertices
	 * @param costs weight for all vertices
	 * @param from first vertex
	 * @param to last vertex
	 * @return a sequence of vertices, or {@code null} if no path exists
	 */
	public static int[] path(int[][] successors, float[] costs, int from, int to) {
		int 		numNodes = successors.length;
		double[] 	distance = new double[numNodes];
		int[] 		bestPred = new int[numNodes];

		for (int i = 0; i<numNodes; ++i){
			// Fill distances with infinity
			distance[i] = Double.POSITIVE_INFINITY;
			// Null best predecessors
			bestPred[i] = -1;
		}

		distance[from] = costs[from];

		
		int actualSuccessor;
		boolean updated = true;
		while (updated){
			updated = false;
			for (int v = 0; v < numNodes; ++v){
				if (successors[v]==null)
					continue;
				for (int n = 0; n < successors[v].length; ++n){
					actualSuccessor = successors[v][n];
					if (distance[actualSuccessor]>distance[v]+costs[actualSuccessor]){
						distance[actualSuccessor]=distance[v]+costs[actualSuccessor];
						bestPred[actualSuccessor]=v;
						updated = true;
					}
				}
			}
		}

		int lenght;
		int currNode = to;
		for (lenght = 1;currNode!=from && lenght <= numNodes && lenght > 0; ++lenght){
			currNode = bestPred[currNode];
			if (currNode < 0)
				return null;
		}

		int[] path = new int[lenght];
		currNode = to;
		while (currNode!=from && lenght <= numNodes && lenght > 0){
			path[--lenght] = currNode;
			currNode = bestPred[currNode];
		}

		return path;
	}

	/**
	 * Find best seam
	 * @param energy weight for all pixels
	 * @return a sequence of x-coordinates (the y-coordinate is the index)
	 */
	public static int[] find(float[][] energy) {
		// Nodes are pixels + source + sink
		int [][] 	successors 	= new int[energy.length * energy[0].length + 2][];
		// Associate a cost for each node
		float [] 	costs 		= new float[energy.length * energy[0].length + 2];

		// Source and Sink positions
		int sourceIndex = successors.length-2;
		int sinkIndex = successors.length-1;

		// Source and Sink nodes have a zero cost
		costs[sourceIndex] = 0;
		costs[sinkIndex] = 0;
		int rows = energy.length;
		int cols = energy[0].length;

		// Define successors for source and sink
		successors[sourceIndex] = new int[cols];
		successors[sinkIndex] 	= new int[cols];
		// For the source we assign all the elements on the first row
		for (int i = 0; i<cols; ++i)
			successors[sourceIndex][i]=i;

		// We assign successors for all energies and fill the costs array
		for (int i = 0; i < rows; ++i){
			for (int j = 0; j < cols; ++j){
				// We also fill costs array
				costs[i*cols+j] = energy[i][j];

				// Do not assign the 3-neighborhood to the last row
				if (i!=(rows-1)){
					successors[i*cols+j] = new int[3];
					for (int hop=-1; hop <=1; ++hop)
						// We want values from the next row, and 3 direct neighbors
						// Invalid border elements will have the same element
						successors[i*cols+j][hop+1] = limitValue(((i+1)*cols+j)+(hop), (i+1)*cols, (i+1)*cols+cols-1);
				} else 
					successors[i*cols+j] = new int[] {sinkIndex};
			}
		}

		// Find the shortest path
		int [] linearPath = path(successors, costs, sourceIndex, sinkIndex);
				
		// If no solutions, return null
		if (linearPath == null || linearPath.length==0) 
			return null;
		// Removing source and sink nodes
		int [] path = new int[linearPath.length-2];
			for (int i = 1; i < linearPath.length - 1; ++i){
			// Remove shift on indexes
			path[i-1] = linearPath[i] - (i-1) * cols;
			}

		// Sanity check, verify we actually made it to the end
		if (path.length != rows)
			return null;

		return path;
	}

	/**
     * Draw a seam on an image
     * @param image original image
     * @param seam a seam on this image
     * @return a new image with the seam in blue
     */
    public static int[][] merge(int[][] image, int[] seam) {
        // Copy image
        int width = image[0].length;
        int height = image.length;
        int[][] copy = new int[height][width];
        for (int row = 0; row < height; ++row)
            for (int col = 0; col < width; ++col)
                copy[row][col] = image[row][col];

        // Paint seam in blue
        for (int row = 0; row < height; ++row)
            copy[row][seam[row]] = 0x0000ff;

        return copy;
    }

    /**
     * Remove specified seam
     * @param image original image
     * @param seam a seam on this image
     * @return the new image (width is decreased by 1)
     */
    public static int[][] shrink(int[][] image, int[] seam) {
        int width = image[0].length;
        int height = image.length;
        int[][] result = new int[height][width - 1];
        for (int row = 0; row < height; ++row) {
            for (int col = 0; col < seam[row]; ++col)
                result[row][col] = image[row][col];
            for (int col = seam[row] + 1; col < width; ++col)
                result[row][col - 1] = image[row][col];
        }
        return result;
    }

}
